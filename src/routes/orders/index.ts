import express from 'express'
const {add, get, getOneOrder, getUserOrder, getPaid, getDelivered, search, delivered} = require('../../controllers/order/index')
const validateResult = require('../../middleware/validateResource')
const {createOrderSchema} = require('../../schema/order.schema')
const requireUser = require('../../middleware/requireUser')

const router = express()

router.post('/add', add)
router.get('/get', get)
router.get('/getOne/:id', getOneOrder)
router.get('/getUserOrder/:id', getUserOrder)
router.get('/getPaid', getPaid)
router.get('/getDelivered', getDelivered)
router.get('/search/:id', search)
router.post('/delivered', delivered)
 
module.exports = router