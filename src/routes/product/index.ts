import express from 'express'
const {addProduct, getProducts, getOneProduct, deleteProduct, getCategory, searchProduct, upadateProduct, getRandomProducts, getRandomCategory, categoryFor} = require('../../controllers/product/index')
const validateResult = require('../../middleware/validateResource')
const {createProductSchema} = require('../../schema/product.schema')
const requireUser = require('../../middleware/requireUser')

const router = express()

router.post('/addProduct', addProduct)
router.get('/getProducts', getProducts)
router.get('/getOneProduct/:id', getOneProduct)
router.delete('/deleteProduct/:id', deleteProduct) 
router.get('/category/:category', getCategory)
router.get('/search/:query', searchProduct)
router.post('/update', upadateProduct)
router.get('/random', getRandomProducts)
router.get('/randomCategory/:category', getRandomCategory)
router.get('/categoryFor/:category/:type', categoryFor)

module.exports = router