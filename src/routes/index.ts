import express from 'express'
const user = require('./user/index')
const product = require('./product/index')
const orders = require('./orders/index')
const post = require('./post/index')

const routes = express()

routes.use('/user', user)
routes.use('/product', product)
routes.use('/order', orders)
routes.use('/post', post)


module.exports = routes