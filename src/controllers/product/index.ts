import express, {query, Request, Response } from 'express'
const db = require('../../utils/connect')
import { createProductInput } from '../../schema/product.schema'

 //insert product
exports.addProduct = async(req: Request, res:Response) =>{
    try{
   const { name, price, quantity, details, description, colors, sizes, categories, type, image1, image2, image3} = req.body
    const priceN = parseInt(price)
    const quantityN = parseInt(quantity)
    const post = {
        name:name,
        price: priceN,
        quantity: quantityN,
        description:description,
        colors:colors,
        sizes:sizes,
        categories:categories,
        details: details,
        type:type,
        image1:image1,
        image2:image2,
        image3:image3
    }
    const sql = 'INSERT INTO products SET ?'
    await db.query(sql, post, (err:any, result:any)=>{
        if(err){
            console.log(err)
            res.status(500).send("An error occured")
        }else{
            res.status(200).send("added")
        }
    })
    }catch(error){
        console.log(error)
    }
}

//get all products
exports.getProducts = async(req:Request,res:Response)=>{
    try{
    const sql = 'SELECT * FROM products'
    await db.query(sql, (err:any, results:any)=>{
        if(err){
            console.log(err)
            return res.status(404).send('No Product found')
        }
        res.status(200).send(results)
    })
    }catch(error){
        console.log(error)
    }
}

//get one product
exports.getOneProduct =  async(req:Request,res:Response)=>{
    try{

    
    const id = req.params.id
    console.log(id)
    const sql =  `SELECT * FROM products WHERE id=${id}`
    await db.query(sql, (err:any, result:any)=>{
        if(err){
            res.status(500).send("Something went wrong")
        }else{
            if(!result[0]){
                return res.status(404).send("Product not found")
            }else{
            console.log(result)
            res.status(200).send(result)
            }
        }
    })
    }catch(err){
        res.status(500).send("Something went wrong")
    }
}

//delete product
exports.deleteProduct =  async(req:Request,res:Response)=>{
    const id = req.params.id
    console.log(id)
    const sql = `DELETE FROM products WHERE id = ${id}`
    await db.query(sql, (err:any, result:any)=>{
        if(err) {
            console.log(err)
            res.status(500).send("Something went wrong")
        }else{
            res.status(200).send("Product has been removed")
        }
    })
}

//get products in a category
exports.getCategory = async(req:Request,res:Response)=>{
    try{
    const {category} = req.params
    console.log(category)
    const sql = `SELECT *
    FROM products
    WHERE categories REGEXP '${category}'`
    await db.query(sql, (err:any, result:any)=>{
        if(err){
            console.log(err)
            res.status(500).send('Something went wrong')
        }else{
            res.status(200).send(result)
        }
    })
    }catch(error){
        console.log(error)
    }
}

//product search
exports.searchProduct = async(req:Request,res:Response)=>{
    try{
    const {query} = req.params
    const sql = `SELECT *
    FROM products
    WHERE description REGEXP '${query}' OR name REGEXP '${query}'` 
    await db.query(sql, (err:any, result:any)=>{
        if(err){
            console.log(err)
            res.status(500).send('Something went wrong')
        }else{
            res.status(200).send(result)
        }
    })
    }catch(error){
        console.log(error)
    }
}

//upadate product 
exports.upadateProduct = async(req:Request,res:Response)=>{

    const {id, name, price, quantity, description, colors, sizes, categories, image1, image2, image3} = req.body
    const sql = `UPDATE products
    SET name = '${name}', price = '${price}', quantity = '${quantity}', description = '${description}', colors = '${colors}', sizes = '${sizes}', categories = '${categories}', image1 = '${image1}', image2 = '${image2}', image3 = '${image3}' WHERE id = ${id}`
    await db.query(sql, (err:any, result:any)=>{
        if(err){
            console.log(err)
            res.status(500).send('Something went wrong')
        }else{
            res.status(200).send("Product Updated ")
        }
    })
}

//get random products
exports.getRandomProducts = async(req:Request,res:Response)=>{
    try{
    const sql = 'SELECT * FROM products ORDER BY RAND() '
    await db.query(sql, (err:any, results:any)=>{
        if(err){
            console.log(err)
            return res.status(404).send('No Product found')
        }
        res.status(200).send(results)
    })
    }catch(error){
        console.log(error)
    }
}

//get random product from categories
exports.getRandomCategory = async(req:Request,res:Response)=>{
    const {category} = req.params
    const sql = `SELECT * FROM products  WHERE categories REGEXP '${category}' ORDER BY RAND()  `
    await db.query(sql, (err:any, results:any)=>{
        if(err){
            console.log(err)
            return res.status(404).send('No Product found')
        }
        res.status(200).send(results)
    })
}

exports.categoryFor = async(req:Request,res:Response)=>{
    const {category, type} = req.params
    const sql = `SELECT * FROM products  WHERE categories REGEXP '${category}' AND type = '${type}' ORDER BY RAND()  `
    await db.query(sql, (err:any, results:any)=>{
        if(err){
            console.log(err)
            return res.status(404).send('No Product found')
        }
        res.status(200).send(results)
    })
}