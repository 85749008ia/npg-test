import express, {query, Request, Response } from 'express'
const db = require('../../utils/connect')
import { createProductInput } from '../../schema/product.schema'
var total:number = 0

exports.add = async(req:Request,res:Response)=>{
    // const sql = 'INSERT INTO orders SET ?'
    var count = 1
    const {user, orderItems, shippingAddress, paymentMethod, itemsPrice, shippingPrice, totalPrice} = req.body
    const price = await orderItems.map(async(item:any) =>{
        try{
            const sql =  `SELECT * FROM products WHERE id=${item.id}`
            await db.query(sql, async(err:any, result:any)=>{
                if(err){
                    console.log(err)
                    res.status(500).send("Something went wrong")
                }else{
                    if(!result[0]){
                        return res.status(400).send("Invalid Product ID")
                    }
                    else{
                    const buffer = {...req.body, totalPrice:result[0].price}
                    const product = result[0].price * item.qty
                    total = total + product
                    if(orderItems.length == count){
                        const sqll = 'INSERT INTO orders SET ?'
 
                        const payload = {...req.body,
                                            // orderItems: '[dd, ddf]',
                                            orderItems: JSON.stringify(orderItems),
                                            itemsPrice:total,
                                            totalPrice:total,
                                            isPaid: false,
                                            isDelivered: false
                            }
                            console.log(payload)
                            await db.query(sqll, payload, (err:any, result:any)=>{
                                if(err){
                                    console.log(err)
                                    res.status(500).send("An error occured")
                                }else{
                                    res.status(200).send("Added")
                                }
                            })
                    }
                    count++
                    }
                }
         })
        }catch(error){
            console.log(error)
            res.status(500).send("Something went wrong2")
        }
    })
    // const payload = {...req.body,
    //                 isPaid: false,
    //                 isDelivered: false
    // }
    // await db.query(sql, req.body, (err:any, result:any)=>{
    //     if(err){
    //         console.log(err)
    //         res.status(500).send("An error occured")
    //     }else{
    //         res.status(200).send("Added")
    //     }
    // })
}

exports.get = async(req:Request,res:Response)=>{
    const sql = 'SELECT * FROM orders'
    await db.query(sql, (err:any, results:any)=>{
        if(err){
            console.log(err)
            return res.status(404).send('No paid order found')
        }
        res.status(200).send(results)
    })
}

exports.getOneOrder =  async(req:Request,res:Response)=>{
    try{

    
    const id = req.params.id
    console.log(id)
    const sql =  `SELECT * FROM orders WHERE id=${id}`
    await db.query(sql, (err:any, result:any)=>{
        if(err){
            res.status(500).send("Something went wrong")
        }else{
            console.log(result)
            res.status(200).send(result)
        }
    })
    }catch(err){
        res.status(500).send("Something went wrong")
    }
}

exports.getUserOrder =  async(req:Request,res:Response)=>{
    try{

    
    const id = req.params.id
    console.log(id)
    const sql =  `SELECT * FROM orders WHERE user=${id}`
    await db.query(sql, (err:any, result:any)=>{
        if(err){
            res.status(500).send("Something went wrong")
        }else{
            console.log(result)
            res.status(200).send(result)
        }
    })
    }catch(err){
        res.status(500).send("Something went wrong")
    }
}

exports.getPaid = async(req:Request, res:Response) =>{
    try{

        const sql = 'SELECT * FROM orders  WHERE isPaid = 1'
        await db.query(sql, (err:any, results:any)=>{
        if(err){
            console.log(err)
            return res.status(404).send('No paid order found')
        }
        res.status(200).send(results)
    })
    }catch(error){
        res.status(500).send("Something went wrong")
    }
}

exports.getDelivered = async(req:Request, res:Response) =>{
    try{

        const sql = 'SELECT * FROM orders  WHERE isDelivered = 1'
        await db.query(sql, (err:any, results:any)=>{
        if(err){
            console.log(err)
            return res.status(404).send('No paid order found')
        }
        res.status(200).send(results)
    })
    }catch(error){
        console.log(error)
        res.status(500).send("Something went wrong")
    }
}

exports.search = async(req:Request, res:Response) =>{
    try{

        const id = req.params.id
        console.log(id)
        const sql =  `SELECT * FROM orders WHERE id REGEXP ${id}`
        await db.query(sql, (err:any, result:any)=>{
            if(err){
                res.status(500).send("Something went wrong")
            }else{
                console.log(result)
                res.status(200).send(result)
            }
        })
    }catch(error){
        console.log(error)
        res.status(500).send("Something went wrong")
    }
}

exports.delivered = async(req:Request, res:Response) =>{

    try{
        const sql = `UPDATE orders
        SET isDelivered = ${req.body.status} WHERE id = ${req.body.id}` 
        await db.query(sql, (err:any, result:any)=>{
            if(err){
                console.log(err)
                res.status(500).send("Somthing went wrong")
            }else{
                res.status(200).send("Edited")
            }
        })
    }catch(error){
        console.log(error)
        res.status(500).send("something went wrong")
    }
}